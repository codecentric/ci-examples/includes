# includes

Mit includes lassen sich komplexe GitLab-CI Pipelines in mehrere Dateien aufspalten.
Die einzelnen YAML Dateien werden beim zusammensetzen `deep merged`.

Dokumentation: https://docs.gitlab.com/ee/ci/yaml/README.html#include
Weitere Beispiele: https://docs.gitlab.com/ee/ci/yaml/includes.html
